import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: 'stats'})
export class Stat {

    @PrimaryGeneratedColumn()
    public readonly id: number;

    @Column({type: 'varchar', length: 12})
    public method: string;

    @Column({type: 'varchar', length: 255})
    public path: string;

    @Column({type: 'varchar', length: 255}) // For IPv6
    public ip: string;

    @Column({type: 'varchar', length: 255, name: 'user_agent'})
    public userAgent: string;

    @Column({type: 'integer', unsigned: true, name: 'http_code'})
    public httpStatusCode: number;

    @Column({type: 'varchar', length: 64, name: 'http_message'})
    public httpStatusMessage: string;

    @Column({type: 'datetime', name: 'access_time'})
    public accessTime: Date;

}
