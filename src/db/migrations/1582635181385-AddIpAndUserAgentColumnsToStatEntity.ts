import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddIpAndUserAgentColumnsToStatEntity1582635181385 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('stats', [
            new TableColumn({
                name: 'ip', type: 'varchar', length: '255', isNullable: true
            }),
            new TableColumn({
                name: 'user_agent', type: 'varchar', length: '255', isNullable: true
            })
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('stats', 'user_agent');
        await queryRunner.dropColumn('stats', 'ip');
    }

}
