import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialStatMigration1582629101564 implements MigrationInterface {
    name = 'InitialStatMigration1582629101564'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "stats" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "path" varchar(255) NOT NULL, "http_code" integer NOT NULL, "http_message" varchar(64) NOT NULL, "access_time" datetime NOT NULL)`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "stats"`, undefined);
    }

}
