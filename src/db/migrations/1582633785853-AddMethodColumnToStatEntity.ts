import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddMethodColumnToStatEntity1582633785853 implements MigrationInterface {
    name = 'AddMethodColumnToStatEntity1582633785853'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('stats', new TableColumn({
            name: 'method', type: 'varchar', length: '12', isNullable: true
        }));

        await queryRunner.query('UPDATE "stats" SET "method"=\'GET\' WHERE "method" IS NULL');

        await queryRunner.changeColumn('stats', 'method', new TableColumn({
            name: 'method', type: 'varchar', isNullable: false
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('stats', 'method');
    }

}
