import {createPool} from 'mysql2/promise';

export const pool = createPool({
    host: process.env.RESTAPI_DB_HOST,
    user: process.env.RESTAPI_DB_USER,
    password: process.env.RESTAPI_DB_PASS,
    database: process.env.RESTAPI_DB_NAME,
    connectionLimit: 30,
    timezone: 'local'
});
