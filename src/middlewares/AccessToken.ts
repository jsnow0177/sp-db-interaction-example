import {NextFunction, Request, RequestHandler, Response} from 'express';
import {pool} from '../Database';
import * as Logger from '../Logger';

const logger = Logger.make('access-token');

interface ClientSession{
    session_id: string;
    user_id: string;
    client_id: string;
    owner_type: string;
    owner_id: string;
    client_redirect_uri: string;
}

/**
 * @returns {RequestHandler}
 */
export const AccessToken = (): RequestHandler => {

    /**
     * @param {string} access_token
     * @returns {Promise<boolean>}
     */
    const isAccessTokenExistsAndNotExpired = (access_token: string): Promise<boolean> => new Promise<boolean>(async resolve => {
        logger.debug(`Check access token ${access_token}`);
        let rows: any[], fields;

        try{
            [rows, fields] = await pool.execute('SELECT * FROM `oauth_access_tokens` WHERE `id`=?', [access_token]);
        }catch(err){
            logger.error(`Error occurred during access token validation`, err);
            return resolve(false);
        }

        if(!rows || !rows[0])
            return resolve(false);

        const expire_time = new Date(rows[0]['expire_time'] * 1000);
        const now = new Date();

        if(now.getTime() >= expire_time.getTime())
            return resolve(false);

        resolve(true);
    });

    /**
     * @param {string} access_token
     * @returns {Promise<ClientSession | null>}
     */
    // @ts-ignore
    const getClientSession = (access_token: string): Promise<ClientSession | null> => new Promise<ClientSession | null>(async resolve => {
        logger.debug(`Get client session for access token ${access_token}`);
        let rows: any[], fields;

        try{
            [rows, fields] = await pool.execute('SELECT oat.session_id, oc.user_id, os.client_id, os.owner_type, os.owner_id, os.client_redirect_uri FROM oauth_access_tokens oat'
                + ' LEFT JOIN oauth_sessions os ON (os.id=oat.session_id)'
                + ' LEFT JOIN oauth_clients oc ON(oc.id=os.client_id)'
                + ' WHERE oat.id=? LIMIT 0, 1', [access_token]);
        }catch(err){
            logger.error(`Error occurred during user data querying`, err);
            return resolve(null);
        }

        if(!rows || !rows[0])
            return resolve(null);

        resolve(<ClientSession>{
            session_id: rows[0]['session_id'],
            user_id: rows[0]['user_id'],
            client_id: rows[0]['client_id'],
            owner_type: rows[0]['owner_type'],
            owner_id: rows[0]['owner_id'],
            client_redirect_uri: rows[0]['client_redirect_uri'],
        });
    });

};
